;My second assembler program is
;
.include "8515def.inc"    ;include the 8515 definition file
.def temp = r16           ;define a temporary register

;
;Continually read in from PORTA and write out to PORTB
;
RESET:
      ;Let's set the Data Direction Registers (DDRA & DDRB)
      ;(0's=inputs, 1's=outputs)
      ldi temp, 0x00
      out DDRA, temp
      ldi temp, 0xFF
      out DDRB, temp
LOOP:
      ;Now, we continually loop, reading from PORTA pins,
      ;negating the value and writing to PORTB
      in    temp, PINA                   ;read in from PORT's input pins
      neg   temp                         ;negate temp register
      out   PORT, temp                   ;write out to PORTB
      rjmp  LOOP                         ;jump back to LOOP
      
