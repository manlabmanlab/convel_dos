.include "8515def.inc";      include the 8515 definition file
.def temp = r16          ;   define a temporary register
;in this example, we will output values to PORTB
;
RESET:
  ;Let's set the Data Direction Register for PORTB (DDRB)
  ;(0=input , 1=output)
  ;pin nums:76543210
  ;         ||||||||
  ;         VVVVVVVV
  ldi temp, 0b11111111      ;this could also be 0xFF
  out DDRB, temp,           ;output the value to DDRB
  ldi temp, 0x01            ;load 1 into temp
LOOP:
  ;Now, we continually loop writing to output B.
  ;followed by rotating left once, and loop back.
  out PORT, temp            ;output temp to PORTB.
  rol temp                  ;rotate temp left.
  rjmp LOOP                 ;jump back to LOOP.
