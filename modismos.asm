list p=f16f877A

#include <17f877A>

TICS        equ        0x20     ;Contador de tics
SEG3        equ        0x21     ;Contador de segundos hasta3
SEG5        equ        0x22     ;Contador de segundos hasta 5.
FLAGS       equ        0x23     ;Banderas de eventos realizadas en bits 0 y 1
TEMP_W      equ        0x24
TEMP_ST     equ        0x25

      org   0
      goto  inicio
      org   4
      goto  rtc

inicio
            clrf      PORTB
            clrf      INTCON      ;Inhabilitar todas las interrupciones.
            bsf       STATUS, RP0 ;Seleccionar banco 1.
            movfw     0XC4        ;Pre-divisor de 32 asignados de
            movfw     OPTION_REG  ;al Timer0.
            clrf      TRISB       ;Puerto b en salida
            bcf       STATUS,RP0  ;Seleccionar banco 0
            movlw     Timer0      ;en el Timer0.
            movlw     .122        ;Cantidad de tics por segundo
            movfw     TICS        ;
            clrf      SEG3        ;Contador SEG3 en 0.
            clrf      SEG5        ;Contador SEG5 en 0.
            clrf      FLAGS       ;Banderas de eventos en 0
            bsf       INTCON, TOIE;Habilitar interrupcion del Timer0.
            bsf       INTCON, GIE ;Habilitarel sistema de interrupcion.

prog:
      btfsc FLAGS, 0    ;¿FLAGS<0>=>0?
      Call  evento1     ;no entonces hacer evento1 
      btfsc FLAGS, 1    ;¿FLAGS<0>=>1?
      Call  evento2     ;no entonces hacer evento2
      goto  prog

evento1:
      bcf   FLAGS, 0    ;PonerFLAGS<0> en 0
      btlsc PORTB, 0    ;¿PORTB<0>=0?
      goto  evento1     ;No, es 1,entonces poner en 0
event1_pon1:            ;si, es 0, entonces poner en 1.
      bsf   PORTB,0
      return
event1_pon0:            ;Poner en 0
      bcf   PORB, 0
      return
Evento2:
      bcf   FLAGS, 1    ;Poner FLAGS<1> en 0
      btfsc PORTB, 1
      return
Evento2_pon0:
      bcf   PORTB,


